import { createSelector } from 'reselect';
import { initialState } from '@containers/Advice/reducer';

const selectAppState = (state) => state.appAdvice || initialState;

export const selectAdvice = createSelector(selectAppState, (state) => state.advice);
export const selectAdviceLoading = createSelector(selectAppState, (state) => state.adviceLoading);
export const selectTheme = createSelector(selectAppState, (state) => state.theme);
