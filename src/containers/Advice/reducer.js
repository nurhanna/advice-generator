import { produce } from 'immer';

import { SET_ADVICE, SET_ADVICE_LOADING } from '@containers/Advice/constants';
import { SET_THEME } from '@containers/App/constants';

export const initialState = {
  advice: [],
  adviceLoading: false,
  theme: 'dark',
};

export const storedKey = ['advice'];

const adviceReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_THEME:
        draft.theme = action.theme;
        break;
      case SET_ADVICE:
        draft.advice = action.advice;
        break;
      case SET_ADVICE_LOADING:
        draft.adviceLoading = action.adviceLoading;
        break;
    }
  });

export default adviceReducer;
