import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_ADVICE } from '@containers/Advice/constants';
import { setAdvice } from '@containers/Advice/actions';
import { setAdviceLoading } from '@containers/Advice/actions';
import { getAdvice } from '@domain/api';
export function* doGetAdvice() {
  yield put(setAdviceLoading(true));
  try {
    console.log('panggil getAdvice api');
    const advice = yield call(getAdvice);
    if (advice) {
      console.log('setAdvice');
      yield put(setAdvice(advice));
    }
  } catch (err) {
    console.log(err);
  }
  yield put(setAdviceLoading(false));
}

export default function* appSaga() {
  console.log('take Latest');
  yield takeLatest(GET_ADVICE, doGetAdvice);
}
