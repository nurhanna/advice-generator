import { SET_ADVICE, GET_ADVICE, SET_ADVICE_LOADING } from '@containers/Advice/constants';
import { SET_THEME } from '@containers/App/constants';

export const setTheme = (theme) => ({
  type: SET_THEME,
  theme,
});

export const setAdvice = (advice) => ({
  type: SET_ADVICE,
  advice,
});

export const getAdvice = () => ({
  type: GET_ADVICE,
});

export const setAdviceLoading = (adviceLoading) => ({
  type: SET_ADVICE_LOADING,
  adviceLoading,
});
