import classes from './style.module.scss';
import refresh from '@static/images/refresh.png';
import { createStructuredSelector } from 'reselect';
import PauseRoundedIcon from '@mui/icons-material/PauseRounded';
import PropTypes from 'prop-types';
import { connect, useDispatch } from 'react-redux';
import { useEffect, useCallback, useState } from 'react';

import { selectTheme } from '@containers/Advice/selectors';
import { selectAdvice } from '@containers/Advice/selectors';
import { selectAdviceLoading } from '@containers/Advice/selectors';
import { getAdvice, setTheme } from '@containers/Advice/actions';

// mui
import CircularProgress from '@mui/material/CircularProgress';
import LinearProgress from '@mui/material/LinearProgress';
import LightModeIcon from '@mui/icons-material/LightMode';
import DarkModeIcon from '@mui/icons-material/DarkMode';

const Advice = ({ adviceData, adviceLoading, theme }) => {
  const dispatch = useDispatch();

  // for trigger refresh
  const [refreshAdvice, setRefreshAdvice] = useState(false);
  const handleRefresh = () => {
    refreshAdvice === true ? setRefreshAdvice(false) : setRefreshAdvice(true);
  };

  // get data from api
  const getData = useCallback(() => {
    dispatch(getAdvice());
  }, [dispatch, refreshAdvice]);

  useEffect(() => {
    getData();
  }, [getData]);

  // theme
  const handleTheme = () => {
    dispatch(setTheme(theme === 'dark' ? 'light' : 'dark'));
  };
  return (
    <div className={classes.containerBase} theme={theme}>
      <div className={classes.theme} onClick={handleTheme}>
        {theme === 'dark' ? (
          <LightModeIcon color="success" className={classes.icon} />
        ) : (
          <DarkModeIcon color="action" className={classes.icon} />
        )}
      </div>
      {Object.keys(adviceData).length > 0 ? (
        <div className={classes.containerAdvice}>
          <div className={classes.adviceNumber}>
            Advice # {adviceLoading === true ? <LinearProgress color="success" /> : adviceData?.slip.id}
            {console.log('data dari api : ', adviceData)}
          </div>
          <div className={classes.adviceText}>
            {adviceLoading === true ? <CircularProgress color="success" /> : adviceData?.slip.advice}
          </div>
          <div className={classes.adviceLine}>
            <div className={classes.line}>
              <hr />
            </div>
            <PauseRoundedIcon />
            <div className={classes.line}>
              <hr />
            </div>
          </div>
          <div className={classes.refreshAdvice} onClick={handleRefresh}>
            <img className={classes.imageRefresh} src={refresh}></img>
          </div>
        </div>
      ) : (
        <div className={classes.containerAdvice}>
          <div className={classes.adviceNumber}>
            Advice # <LinearProgress color="success" />
          </div>
          <div className={classes.adviceText}>
            <CircularProgress color="success" />
          </div>
          <div className={classes.adviceLine}>
            <div className={classes.line}>
              <hr />
            </div>
            <PauseRoundedIcon />
            <div className={classes.line}>
              <hr />
            </div>
          </div>
          <div className={classes.refreshAdvice} onClick={handleRefresh}>
            <img className={classes.imageRefresh} src={refresh}></img>
          </div>
        </div>
      )}
    </div>
  );
};

// export default Advice;
const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
  adviceData: selectAdvice,
  adviceLoading: selectAdviceLoading,
});

Advice.propTypes = {
  theme: PropTypes.string,
  adviceData: PropTypes.array,
  adviceLoading: PropTypes.bool,
};
const mapDispatchToProps = {
  setTheme,
};

export default connect(mapStateToProps, mapDispatchToProps)(Advice);
