import MainLayout from '@layouts/MainLayout';

import NotFound from '@pages/NotFound';
import Advice from '@pages/Advice';

const routes = [
  {
    path: '/',
    name: 'Advice',
    component: Advice,
  },

  { path: '*', name: 'Not Found', component: NotFound, layout: MainLayout },
];

export default routes;
