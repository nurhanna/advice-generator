import { all } from 'redux-saga/effects';

import appSaga from '@containers/Advice/saga';

export default function* rootSaga() {
  console.log('hello root');
  yield all([appSaga()]);
}
